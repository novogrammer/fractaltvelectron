import {
  IS_STANDALONE,
} from "../constants";


import electron from "electron";
const {
  app,
  BrowserWindow,
  ipcMain,
  shell,
  dialog,
}=electron;

import path from "path";
import url from "url";
import os from "os";
import EventEmitter from "eventemitter3";


import CommunicationContext from "./Communication/CommunicationContext";

import {
  IS_DEBUG,
  WINDOW_WIDTH,
  WINDOW_HEIGHT,
} from "../constants";

import PacketConverter from "./PacketConverter";

export default class MainApp{
  constructor(){
    let mainWindow=null;
    let emitter=new EventEmitter();

    function createWindow () {
      mainWindow = new BrowserWindow({
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
        //kiosk:true,
      });
      mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, '../RendererProcess/index.html'),
        protocol: 'file:',
        slashes: true,
      }));
      
      mainWindow.webContents.on('new-window',function(event,url){
        event.preventDefault();
        shell.openExternal(url);
      });
      
      emitter.on("errorDialog",(message)=>{
        dialog.showMessageBox(mainWindow,{
          message:message,
        });
      });
      let packetConverter=new PacketConverter(emitter,mainWindow.webContents);
      
      
      if(IS_DEBUG){
        emitter.on("packetFromArduinoReArray",(packet)=>{
          console.log(packet);
        });
        emitter.on("packetFromArduinoSwArray",(packet)=>{
          console.log(packet);
        });
      }
      if(!IS_STANDALONE){
        let communicationContext=new CommunicationContext(emitter);
      }else{
        console.log("IS_STANDALONE");
        setTimeout(()=>{
          mainWindow.webContents.send("switchChanged",1,true);//power
          mainWindow.webContents.send("switchChanged",0,true);//automatic
        },2000);
      }
      
      
      if(IS_DEBUG){
        mainWindow.webContents.openDevTools();
      }

      //mainWindow.on('closed', function () {
      //  mainWindow = null;
      //});
    }

    app.on('ready', createWindow);

    app.on('window-all-closed', function () {
      //if (process.platform !== 'darwin') {
        app.quit();
      //}
    });

    /*
    app.on('activate', function () {
      if (mainWindow === null) {
        createWindow();
      }
    });
    */    
  }
}