

import {
  IS_DEBUG,
  RE_SCALE,
} from "../constants";

import{
  unsignedToSigned,
} from "../math_utils";

export default class PacketConverter{
  constructor(emitter,webContents){
    this.emitter=emitter;
    this.webContents=webContents;
    this.reset();
    this.setupEvent();
    
  }
  reset(){
    this.reArray=[0,0,0,0,0,0];
    this.swArray=[0,0];
  }
  setupEvent(){
    this.emitter.on("packetFromArduinoReArray",this.onPacketFromArduinoReArray,this);
    this.emitter.on("packetFromArduinoSwArray",this.onPacketFromArduinoSwArray,this);
    
  }
  onPacketFromArduinoReArray(packet){
    let length=Math.min(this.reArray.length,packet.reArray.length);
    for(let i=0;i<length;++i){
      let reTruncatedPrev=Math.floor(this.reArray[i]*RE_SCALE)/RE_SCALE;
      let reTruncatedCurrent=Math.floor(packet.reArray[i]*RE_SCALE)/RE_SCALE;
      let unsignedDiff=(reTruncatedCurrent-reTruncatedPrev)&0xff;
      let signedDiff=unsignedToSigned(unsignedDiff,8);
      if(signedDiff!=0){
        if(i==0){
          this.webContents.send("renderPassIndexByRelative",signedDiff*RE_SCALE);
        }else{
          this.webContents.send("paramByRelative",i-1,signedDiff*RE_SCALE);
        }
      }
    }
    this.reArray=packet.reArray;
  }
  onPacketFromArduinoSwArray(packet){
    let length=Math.min(this.swArray.length,packet.swArray.length);
    for(let i=0;i<length;++i){
      let swPrev=this.swArray[i];
      let swCurrent=packet.swArray[i];
      if(swPrev!=swCurrent){
        this.webContents.send("switchChanged",i,!!swCurrent);
      }
    }
    this.swArray=packet.swArray;
  }
}
