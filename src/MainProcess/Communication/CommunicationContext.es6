import {
  BAUDRATE,
} from "../../constants";
import SerialPort from 'serialport';
import ByteLength from '@serialport/parser-byte-length';


import CommunicationStateInitializing from "./CommunicationState/CommunicationStateInitializing";

export default class CommunicationContext{
  constructor(emitter){
    this.emitter=emitter;
    this.communicationState=null;
    this.setupSerial();
  }
  setupSerial(){
    this.serialPort=null;
    this.constructor.findArduinoPort()
    .then((port)=>{
      this.serialPort=new SerialPort(port.comName,{
        baudRate:BAUDRATE,
      });
      let parser=this.serialPort.pipe(new ByteLength({length:1}));
      this.serialPort.on('open',()=>{
        console.log("serialPort opened");
        this.communicationState=new CommunicationStateInitializing(this);
      });
      parser.on('data',this.onData.bind(this));
      
    })
    .catch((err)=>{
      if(err.message){
        this.emitter.emit("errorDialog",err.message);
      }else{
        this.emitter.emit("errorDialog","Unknown Error");
      }
    });
  }
  static findArduinoPort(){
    return new Promise((resolve,reject)=>{
      SerialPort.list().then((ports)=>{
        let arduinoPorts=ports.filter((port)=>port.manufacturer=="Arduino (www.arduino.cc)");
        if(arduinoPorts.length==0){
          reject(new Error("arduinoPorts.length==0"));
        }else{
          resolve(arduinoPorts[0]);
        }
      });
    });
  }
  onData(data){
    //console.log(data);
    if(this.communicationState==null){
      console.log("communicationState==null");
      return;
    }
    this.communicationState.onData(data);
  }
}