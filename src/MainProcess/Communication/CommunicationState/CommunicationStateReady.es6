import {
  TYPE_RE_ARRAY01,
  TYPE_RE_ARRAY07,
  TYPE_SW_ARRAY02,
  HEADER_SIZE,
} from "../../../constants";

import CommunicationStateBase from "./CommunicationStateBase";

export default class CommunicationStateReady extends CommunicationStateBase{
  constructor(context){
    super(context);
    this.parsingPacketSize=null;
    this.parsingBytes=[];
    this.parsingPacket=null;
    console.log("CommunicationStateReady");
  }
  onData(data){
    for(let i=0;i<data.length;++i){
      let byte=data.readUInt8(i);
      if(this.parsingPacketSize==null){
        this.parsingPacketSize=byte;
        this.parsingBytes=[];
      }
      this.parsingBytes.push(byte);
      if(this.parsingPacketSize==this.parsingBytes.length){
        debugger;
        let type=this.parsingBytes[1];
        switch(type){
          case TYPE_RE_ARRAY01:
          this.emitPacketReArray({
            size:this.parsingPacketSize,
            type:type,
            reArray:[
              this.parsingBytes[HEADER_SIZE+0],
            ],
          });
          break;
          case TYPE_RE_ARRAY07:
          this.emitPacketReArray({
            size:this.parsingPacketSize,
            type:type,
            reArray:[
              this.parsingBytes[HEADER_SIZE+0],
              this.parsingBytes[HEADER_SIZE+1],
              this.parsingBytes[HEADER_SIZE+2],
              this.parsingBytes[HEADER_SIZE+3],
              this.parsingBytes[HEADER_SIZE+4],
              this.parsingBytes[HEADER_SIZE+5],
              this.parsingBytes[HEADER_SIZE+6],
            ],
          });
          break;
          case TYPE_SW_ARRAY02:
          this.emitPacketSwArray({
            size:this.parsingPacketSize,
            type:type,
            swArray:[
              this.parsingBytes[HEADER_SIZE+0],
              this.parsingBytes[HEADER_SIZE+1],
            ],
          })
          break;
          default:
          this.context.emitter.emit("errorDialog","UknownPacketType:"+byte);
          break;
        }
        this.parsingPacketSize=null;
        this.parsingBytes=[];
      }
    }
    //console.log(data);
  }
  emitPacketReArray(packet){
    this.context.emitter.emit("packetFromArduinoReArray",packet);
  }
  emitPacketSwArray(packet){
    this.context.emitter.emit("packetFromArduinoSwArray",packet);
  }
}