import {
  HANDSHAKE_FROM_PC,
  HANDSHAKE_FROM_ARDUINO,
} from "../../../constants";


import CommunicationStateBase from "./CommunicationStateBase";
import CommunicationStateReady from "./CommunicationStateReady";
export default class CommunicationStateInitializing extends CommunicationStateBase{
  constructor(context){
    super(context);
    this.handshakeFromArduino="";
    console.log("sendHandshake after 2000[ms]")
    setTimeout(this.sendHandshake.bind(this),2000);
  }
  onData(data){
    this.handshakeFromArduino+=data.toString('ascii');
    if(this.handshakeFromArduino==HANDSHAKE_FROM_ARDUINO){
      this.context.communicationState=new CommunicationStateReady(this.context);
    }
  }
  sendHandshake(){
    console.log("sendHandshake");
    this.context.serialPort.flush();
    this.handshakeFromArduino="";
    this.context.serialPort.write(HANDSHAKE_FROM_PC);
    
  }
}