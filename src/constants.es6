//common
export const IS_DEBUG=false;
export const WINDOW_WIDTH=1024;
export const WINDOW_HEIGHT=768;

//main
export const BAUDRATE=115200;
export const HANDSHAKE_FROM_PC="ready?\r\n";
export const HANDSHAKE_FROM_ARDUINO="ok.\r\n";
export const TYPE_RE_ARRAY01=1;
export const TYPE_RE_ARRAY07=7;
export const TYPE_SW_ARRAY02=16+2;
export const HEADER_SIZE=2;
export const RE_SCALE=0.5;
export const IS_STANDALONE=false;

//render
export const FPS=60;
export const POWERON_DURATION=2;
export const POWEROFF_DURATION=0.5;
export const CHANNEL_DURATION=0.4;
//パワーオフしてからパワーオンまでの時間
export const RESET_TIME=3.0;

export const PIXI_FRAG_COMMON=`
precision mediump float;

varying vec2 vTextureCoord;
varying vec4 vColor;

uniform sampler2D uSampler;
uniform vec4 filterClamp;
uniform vec4 filterArea;

vec2 mapCoord( vec2 coord )
{
    coord *= filterArea.xy;
    coord += filterArea.zw;

    return coord;
}

vec2 unmapCoord( vec2 coord )
{
    coord -= filterArea.zw;
    coord /= filterArea.xy;

    return coord;
}
`;