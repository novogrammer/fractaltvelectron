
export function unsignedToSigned(number,bits){
  if(32<bits){
    throw new Error('bitwise operator are converted to signed 32-bit integers');
  }
  let msb=1<<(bits-1);
  
  let mask=~((~0)<<(bits-1));
  if(number & msb){
    return (number&mask)+msb*Math.sign(msb)*-1;
  }else{
    return number;
  }
}
/*
console.assert(unsignedToSigned(0x80,8)==-128);
console.assert(unsignedToSigned(0xff,8)==-1);

console.assert(unsignedToSigned(0x8000,16)==-32768);
console.assert(unsignedToSigned(0xffff,16)==-1);

console.assert(unsignedToSigned(0x80000000,32)==-2147483648);
console.assert(unsignedToSigned(0xffffffff,32)==-1);
*/

export function degToRad(degree){
  return degree*Math.PI/180;
}

export function random(min=null,max=null){
  if(min==null){
    max=1;
    min=0;
  }else if(max==null){
    max=min;
    min=0;
  }
  return Math.random()*(max-min)+min;
  
}

export function circularIndex(length,index){
  return (index%length+length)%length
}
export function circularGetter(buffer,index){
  return buffer[circularIndex(buffer.length,index)];
}

export function map(value,inputMin,inputMax,outputMin,outputMax){
  return (value-inputMin)/(inputMax-inputMin)*(outputMax-outputMin)+outputMin;
}