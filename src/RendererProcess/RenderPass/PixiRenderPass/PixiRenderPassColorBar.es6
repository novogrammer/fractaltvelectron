import PixiRenderPassBase from "./PixiRenderPassBase";

export default class PixiRenderPassColorBar extends PixiRenderPassBase{
  constructor({config}){
    super({config});
    this.setupScene();
  }
  setupScene(){
    //https://ja.wikipedia.org/wiki/%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB:SMPTE_COLOR_BAR_75.png
    this.sprite=PIXI.Sprite.fromImage("SMPTE_COLOR_BAR_75.png");
    this.scene.addChild(this.sprite);
  }
  render(){
    let {width,height}=this.renderTexture;
    this.sprite.width=width;
    this.sprite.height=height;
    super.render();
  }
  setParamByRelative(index,paramByRelative){
    //DO NOTHING
  }
}

