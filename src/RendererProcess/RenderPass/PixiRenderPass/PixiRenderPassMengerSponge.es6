import {
  degToRad,
  circularGetter,
  map,
} from "../../../math_utils.es6";
import {
  PIXI_FRAG_COMMON,
} from "../../../constants.es6";

import PixiRenderPassBase from "./PixiRenderPassBase";
import BeatMaker from "../../BeatMaker";


//for extends
const PIXI=require('pixi.js');

const glsl=require("glslify");

const MAX_DEPTH=7;

class MengerSpongeFilter extends PIXI.Filter{
  constructor(){
    let uniforms={
      time:{type:'1f',value:0},
      depthMax:{type:'1i',value:0},
      rotationX:{type:'1f',value:0},
      rotationY:{type:'1f',value:0},
      cameraX:{type:'1f',value:0},
      cameraY:{type:'1f',value:0},
      cameraZ:{type:'1f',value:0},
    };
    let fragmentShader=glsl(`
${PIXI_FRAG_COMMON}

uniform float time;
uniform int depthMax;
uniform float rotationX;
uniform float rotationY;
uniform float cameraX;
uniform float cameraY;
uniform float cameraZ;


const vec3 PX=vec3(+1.0, 0.0, 0.0);
const vec3 NX=vec3(-1.0, 0.0, 0.0);
const vec3 PY=vec3( 0.0,+1.0, 0.0);
const vec3 NY=vec3( 0.0,-1.0, 0.0);
const vec3 PZ=vec3( 0.0, 0.0,+1.0);
const vec3 NZ=vec3( 0.0, 0.0,-1.0);
const vec3 UP=PY;
const vec3 LIGHT_DIRECTION=normalize(vec3(1.0,1.0,1.0));
const vec3 DIFFUSE_COLOR=vec3(255.0/255.0,143.0/255.0,121.0/255.0);
const vec3 SPECULAR_COLOR=vec3(0.3);
const vec3 AMBIENT_COLOR=vec3(0.1);
const float DISTANCE_MIN=0.0001;
const float SHADOW_LENGTH_MAX=5.0;
const float SHADOW_OFFSET=DISTANCE_MIN*10.0;
const float SHADOW_FACTOR=0.3;
//const float DISTANCE_MAX=1000.0;
const float DISTANCE_MAX=100.0;
const vec3 GROUND_DIFFUSE_COLOR=vec3(189.0/255.0,255.0/255.0,176.0/255.0);
const vec3 GROUND_SPECULAR_COLOR=vec3(0.0,0.0,0.0);
const float GROUND_LEVEL=-2.0;
const vec3 SKY_COLOR=vec3(135.0/255.0,206.0/255.0,235.0/255.0);
const vec3 FOG_COLOR=vec3(1.0);
const float FOG_FAR=20.0;
const float FOG_DENSITY=1.0;
const float FOG_POWER_FACTOR=2.0;

#define STEP_MAX 128

#pragma glslify: rotateX = require(glsl-y-rotate/rotateX)
#pragma glslify: rotateY = require(glsl-y-rotate/rotateY)
#pragma glslify: rotateZ = require(glsl-y-rotate/rotateZ)

float distanceGround(vec3 p,float level){
  return p.y-level;
}
float distanceBox(vec3 p,vec3 size){
  vec3 d=abs(p)-size;
  float innerDistance=max(d.x,max(d.y,d.z));
  return min(innerDistance,length(max(d,0.0)));
}
float distanceBox(vec2 p,vec2 size){
  vec2 d=abs(p)-size;
  float innerDistance=max(d.x,d.y);
  return min(innerDistance,length(max(d,0.0)));
}

float distanceCrossBox(vec3 p,vec3 size){
  float distanceZ=distanceBox(p.xy,size.xy);
  float distanceX=distanceBox(p.yz,size.yz);
  float distanceY=distanceBox(p.zx,size.zx);
  return min(distanceZ,min(distanceX,distanceY));
}

//shadertoy
//https://www.shadertoy.com/view/4sX3Rn
//article
//https://www.iquilezles.org/www/articles/menger/menger.htm
float distanceMengerSponge(vec3 p){
  float distance=distanceBox(p,vec3(1.0));

  float s=1.0;
  for(int m=0;m<10;++m){
    if(depthMax<=m){
      break;
    }
    
    //繰り返された領域に変換し外の枠とANDを取っている。半径1のCrossBoxなので直径は2
    vec3 a=mod(p*s,2.0)-1.0;
    s*=3.0;
    
    vec3 r=abs(1.0-3.0*abs(a));
    float da=max(r.x,r.y);
    float db=max(r.y,r.z);
    float dc=max(r.z,r.x);
    float c=(min(da,min(db,dc))-1.0)/s;
    //以下の式でも同じ
    //float c=distanceCrossBox(r,vec3(1.0))/s;
    distance=max(distance,c);
  }


  return distance;
}

float distanceScene(vec3 p){
  mat3 rotateSponge=rotateX(rotationX)*rotateY(rotationY);
  return min(distanceMengerSponge(rotateSponge*p),distanceGround(p,GROUND_LEVEL));
}

vec3 calcNormal(vec3 p){
  float d=DISTANCE_MIN*2.0;
  return normalize(vec3(
    distanceScene(p+PX*d)-distanceScene(p+NX*d),
    distanceScene(p+PY*d)-distanceScene(p+NY*d),
    distanceScene(p+PZ*d)-distanceScene(p+NZ*d)
  ));
}

vec3 calcColor(vec3 p,vec3 normal,vec3 ray,vec3 diffuseColor,vec3 specularColor){
  if(distanceScene(p)<DISTANCE_MIN*2.0){
    float shininess=30.0;
    float diffuse=max(dot(normal,LIGHT_DIRECTION),0.0);
    vec3 halfway=normalize(LIGHT_DIRECTION-ray);
    float specular=pow(max(dot(normal,halfway),0.0),shininess);
    return diffuseColor*diffuse+specularColor*specular;
  }else{
    //error
    return vec3(1.0,0.0,1.0);
  }
  
}

//https://msdn.microsoft.com/ja-jp/library/ee418606(v=vs.85).aspx
vec3 calcFog(vec3 originalColor,float distance){
  float d=distance/FOG_FAR;
  float f=1.0/exp(pow(d*FOG_DENSITY,FOG_POWER_FACTOR));
  return mix(FOG_COLOR,originalColor,f);
}


//xyz:表面の座標 , w:isHit
vec4 rayMarching(vec3 from,vec3 ray,float maxLength){
  vec3 p=from;
  float rayLength=0.0;
  bool isHit=false;
  for(int i=0;i<STEP_MAX;++i){
    float distance=distanceScene(p);
    rayLength+=distance;
    p=from+ray*rayLength;
    if(distance<DISTANCE_MIN){
      isHit=true;
      break;
    }
    if(maxLength<rayLength){
      break;
    }
  }
  return vec4(p,isHit?1.0:0.0);
}

void main(void)
{
  vec2 mappedCoord=mapCoord(vTextureCoord);
  vec2 centeredUv=(mappedCoord-filterArea.xy/2.0)/filterArea.y;
  //flip Y
  centeredUv*=vec2(1.0,-1.0);
  
  vec3 cameraPosition=vec3(cameraX,cameraY,cameraZ);
  vec3 cameraLookAt=cameraPosition+vec3(0.0,0.0,-1.0);
  vec3 cameraDirection=normalize(cameraLookAt-cameraPosition);
  float focalLength=0.5;
  
  mat3 cameraMatrix=mat3(
    cross(cameraDirection,UP),
    UP,
    cameraDirection
  );
  vec3 ray=normalize(cameraMatrix*vec3(centeredUv,focalLength));
  vec4 result=rayMarching(cameraPosition,ray,DISTANCE_MAX);
  bool isHit=result.w!=0.0;
  
  if(isHit){
    vec3 p=result.xyz;
    vec3 normal=calcNormal(p);
    
    vec4 shadowResult=rayMarching(p+normal*SHADOW_OFFSET,LIGHT_DIRECTION,SHADOW_LENGTH_MAX);
    
    bool isShadow=shadowResult.w!=0.0;
    float shadowFactor=isShadow?SHADOW_FACTOR:1.0;
    
    vec3 color;
    if(distanceGround(p,GROUND_LEVEL)<DISTANCE_MIN){
      color=calcColor(p,normal,ray,GROUND_DIFFUSE_COLOR,GROUND_SPECULAR_COLOR);
    }else{
      color=calcColor(p,normal,ray,DIFFUSE_COLOR,SPECULAR_COLOR);
    }
    color*=shadowFactor;
    color+=AMBIENT_COLOR;
    color=calcFog(color,length(p-cameraPosition));
    
    gl_FragColor=vec4(color,1.0);
    
  }else{
    float skyRatio=max(centeredUv.y*2.0,0.0);
    gl_FragColor=vec4(mix(FOG_COLOR,SKY_COLOR,skyRatio),1.0);
  }
}

`);
    super(null,fragmentShader,uniforms);
    this.padding=0;
  }
  
}



export default class PixiRenderPassMengerSponge extends PixiRenderPassBase{
  constructor({config}){
    super({config});
    this.beatMaker=new BeatMaker({
      bpm:120,
      timeStart:this.timeNow,
    });
    this.previousBeat=this.beatMaker.getBeat();
    this.resetParams();
    this.setupScene();
  }
  resetParams(){
    this.depthMax=5;
    this.rotationX=0;
    this.rotationY=0;
    this.cameraX=0;
    this.cameraY=0;
    this.cameraZ=3;
  }
  setupScene(){
    this.g=new PIXI.Graphics();
    this.scene.addChild(this.g);
    this.mengerSpongeFilter=new MengerSpongeFilter();
    this.g.filters=[this.mengerSpongeFilter];
    
    this.folder=this.gui.addFolder("PixiRenderPassMengerSponge");
    this.folder.open();
    this.setupSliders([
      {
        name:"depthMax",
        min:0,
        max:MAX_DEPTH,
        step:1,
      },
      {
        name:"rotationX",
        min:-180,
        max:180,
        step:5,
      },
      {
        name:"rotationY",
        min:-180,
        max:180,
        step:5,
      },
      {
        name:"cameraX",
        min:-2,
        max:2,
        step:0.1,
      },
      {
        name:"cameraY",
        min:-2,
        max:2,
        step:0.1,
      },
      {
        name:"cameraZ",
        min:-2,
        max:5,
        step:0.1,
      },
    ]);
  }
  update(timeNow){
    this.beatMaker.update(timeNow);
    let beat=this.beatMaker.getBeat();
    if(this.isAutomatic){
      this.rotationX=degToRad(timeNow/(beat.spb*16)*360);
      this.rotationY=degToRad(timeNow/(beat.spb*8)*360);
      if(beat.count!=this.previousBeat.count){
        let pattern=[
          7,6,5,7,
          6,5,4,6,
          5,4,3,5,
          4,3,2,4,
          3,2,1,3,
          2,1,0,2,
          0,1,2,3,
          4,5,6,7,
        ];
        let step=circularGetter(pattern,beat.count);
        this.depthMax=step;
      }
    }
    
    this.previousBeat=beat;
  }
  
  render(){
    let {width,height}=this.renderTexture;
    this.g.clear();
    this.g.beginFill(0xffffff);
    this.g.drawRect(0,0,width,height);
    this.mengerSpongeFilter.uniforms.time=this.timeNow;
    this.mengerSpongeFilter.uniforms.depthMax=this.depthMax;
    this.mengerSpongeFilter.uniforms.rotationX=degToRad(this.rotationX);
    this.mengerSpongeFilter.uniforms.rotationY=degToRad(this.rotationY);
    this.mengerSpongeFilter.uniforms.cameraX=this.cameraX;
    this.mengerSpongeFilter.uniforms.cameraY=this.cameraY;
    this.mengerSpongeFilter.uniforms.cameraZ=this.cameraZ;
    super.render();
  }
  
}

