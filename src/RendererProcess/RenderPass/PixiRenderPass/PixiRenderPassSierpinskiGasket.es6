import {
  degToRad,
  circularGetter,
  map,
} from "../../../math_utils.es6";

import PixiRenderPassBase from "./PixiRenderPassBase";
import BeatMaker from "../../BeatMaker";

import FlyweightFactory from "../../FlyweightFactory";
const LENGTH=300;
const COLOR_FROM=0xff0000;
const MAX_DEPTH=5;


export default class PixiRenderPassSierpinskiGasket extends PixiRenderPassBase{
  constructor({config}){
    super({config});
    this.beatMaker=new BeatMaker({
      bpm:280,
      timeStart:this.timeNow,
    });
    this.previousBeat=this.beatMaker.getBeat();
    this.tween=new TWEEN.Tween(this)
    .easing(TWEEN.Easing.Elastic.Out);
    this.resetParams();
    this.setupScene();
  }
  resetParams(){
    this.maxDepth=MAX_DEPTH;
    this.polygon=3;
    this.offsetAngle=0;
    this.offsetY=0;
    this.offsetX=0;
    //this.brighten=0;
    this.hue=30;
    this.colorFlyweightFactory=new FlyweightFactory(function({hue,depth}){
      let color=chroma(COLOR_FROM)
      .set("hsl.h",(hue*depth)%360)
      .num();
      return color;
      
    },function(keyA,keyB){
      return keyA.hue==keyB.hue && keyA.depth==keyB.depth;
    });
  }
  setupScene(){
    let nativeLines=true;
    this.g=new PIXI.Graphics(nativeLines);
    this.scene.addChild(this.g);
    this.folder=this.gui.addFolder("PixiRenderPassSierpinskiGasket");
    this.folder.open();
    this.setupSliders([
      {
        name:"maxDepth",
        min:0,
        max:MAX_DEPTH,
        step:1,
      },
      {
        name:"polygon",
        min:3,
        max:4,
        step:1,
      },
      {
        name:"offsetAngle",
        min:-180,
        max:180,
        step:5,
      },
      {
        name:"offsetY",
        min:-300,
        max:300,
        step:5,
      },
      {
        name:"offsetX",
        min:-300,
        max:300,
        step:5,
      },
      /*
      {
        name:"brighten",
        min:0,
        max:2,
        step:0.1,
      },
      */
      {
        name:"hue",
        min:0,
        max:360,
        step:5,
      },
    ]);
  }
  //triangle or polygon
  renderTriangle(depth=0){
    let halfWay=(this.polygon-1)/2;
    let color=this.colorFlyweightFactory.getValue({
      hue:this.hue,
      depth:depth,
    });
    this.g.lineStyle(1,color,1,1);
    for(let i=0;i<=this.polygon;++i){
      this.pushMatrix();
      let m=new PIXI.Matrix();
      m.rotate(degToRad(360/this.polygon*(i-halfWay)));
      this.matrix.append(m);
      let point=new PIXI.Point(0,-LENGTH);
      point=this.matrix.apply(point);
      if(i==0){
        this.g.moveTo(point.x,point.y);
      }else{
        this.g.lineTo(point.x,point.y);
      }
      this.popMatrix();
    }
    //this.g.closePath();
    
    if(depth<this.maxDepth){
      for(let i=0;i<this.polygon;++i){
        this.pushMatrix();
        let m=new PIXI.Matrix();
        m.scale(0.5,0.5);
        m.translate(this.offsetX,(LENGTH+this.offsetY)*-0.5);
        m.rotate(degToRad((360/this.polygon+this.offsetAngle)*(i-halfWay)));
        this.matrix.append(m);
        this.renderTriangle(depth+1);
        this.popMatrix();
      }
    }
  }
  update(timeNow){
    this.beatMaker.update(timeNow);
    let beat=this.beatMaker.getBeat();
    if(this.isAutomatic){
      let radX=degToRad(timeNow/(beat.spb*8)*360);
      this.offsetX=Math.cos(radX)*25;
      if(beat.count!=this.previousBeat.count){
        //this.hue= Math.round(Math.sin(degToRad(timeNow/(beat.spb*16)*360))*180+180,-1);
        let pattern=[
          50,50, 0, 0,50,50, 0, 0,
          50,50, 0, 0,50,50, 0, 0,
          50,50, 0, 0,50,50, 0, 0,
          50, 0,100, 0,50, 0,100, 0,
        ];
        let step=circularGetter(pattern,beat.count);
        this.tween.to({offsetY:step},beat.spb*1000).start();
        this.branchAngle=step;
      }
    }
    
    this.previousBeat=beat;
  }
  render(){
    let width=this.renderTexture.width;
    let height=this.renderTexture.height;
    this.g.clear();
    //this.g.beginFill(0xffffff);
    this.clearStacks();
    this.pushMatrix();
    let m=new PIXI.Matrix();
    m.translate(width/2,height/2);
    this.matrix.append(m);
    this.renderTriangle();
    this.popMatrix();
    super.render();
  }
  
}

