import {
  degToRad,
  circularGetter,
  map,
} from "../../../math_utils.es6";
import {
  PIXI_FRAG_COMMON,
} from "../../../constants.es6";


import PixiRenderPassBase from "./PixiRenderPassBase";
import BeatMaker from "../../BeatMaker";


//for extends
const PIXI=require('pixi.js');

const glsl=require("glslify");


class NoiseFilter extends PIXI.Filter{
  constructor(){
    let uniforms={
      time:{type:'1f',value:0},
      
    };
    let fragmentShader=glsl(`
${PIXI_FRAG_COMMON}

uniform float time;

#pragma glslify: cnoise3 = require(glsl-noise/classic/3d)

void main(void)
{
  vec2 mappedCoord=mapCoord(vTextureCoord);
  vec2 centeredUv=(mappedCoord-filterArea.xy/2.0)/filterArea.y;
  vec3 noiseParam=vec3(centeredUv*vec2(400.0,1000.0+time*1000.0),time*1000.0);
  vec3 color=vec3(cnoise3(noiseParam)+1.0)*0.5;
  gl_FragColor=vec4(color,1.0);
}

`);
    super(null,fragmentShader,uniforms);
    this.padding=0;
  }
  
}



export default class PixiRenderPassNoise extends PixiRenderPassBase{
  constructor({config}){
    super({config});
    this.resetParams();
    this.setupScene();
  }
  resetParams(){
  }
  setupScene(){
    this.g=new PIXI.Graphics();
    this.scene.addChild(this.g);
    this.noiseFilter=new NoiseFilter();
    this.g.filters=[this.noiseFilter];
  }
  
  render(){
    let {width,height}=this.renderTexture;
    this.g.clear();
    this.g.beginFill(0xffffff);
    this.g.drawRect(0,0,width,height);
    this.noiseFilter.uniforms.time=this.timeNow;
    super.render();
  }
  setParamByRelative(index,paramByRelative){
    //DO NOTHING
  }
  
}

