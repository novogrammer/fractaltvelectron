
vec3 fromHex(int r,int g,int b){
  return vec3(
    float(r)/255.0,
    float(g)/255.0,
    float(b)/255.0
  );
}

vec3 getColor0(int iteration){
  float m=mod(float(iteration),6.0);
  if(m==0.0)return vec3(1.0,0.0,0.0);
  if(m==1.0)return vec3(1.0,1.0,0.0);
  if(m==2.0)return vec3(0.0,1.0,0.0);
  if(m==3.0)return vec3(0.0,1.0,1.0);
  if(m==4.0)return vec3(0.0,0.0,1.0);
  if(m==5.0)return vec3(1.0,0.0,1.0);
  return vec3(1.0);//dont mind
}
vec3 getColor1(int iteration){
  float m=mod(float(iteration),6.0);
  if(m==0.0)return fromHex(0x68,0x9f,0x38);
  if(m==1.0)return fromHex(0xdc,0xed,0xc8);
  if(m==2.0)return fromHex(0x8b,0xc3,0x4a);
  if(m==3.0)return fromHex(0x00,0xbc,0xd4);
  if(m==4.0)return fromHex(0x75,0x75,0x75);
  if(m==5.0)return fromHex(0xbd,0xbd,0xbd);
  return vec3(1.0);//dont mind
}
vec3 getColor2(int iteration){
  float m=mod(float(iteration),6.0);
  if(m==0.0)return fromHex(0x7b,0x1f,0xa2);
  if(m==1.0)return fromHex(0xe2,0xbe,0xe7);
  if(m==2.0)return fromHex(0x9c,0x27,0xb0);
  if(m==3.0)return fromHex(0xff,0x57,0x22);
  if(m==4.0)return fromHex(0x75,0x75,0x75);
  if(m==5.0)return fromHex(0xbd,0xbd,0xbd);
  return vec3(1.0);//dont mind
}
vec3 getColor3(int iteration){
  float m=mod(float(iteration),6.0);
  if(m==0.0)return fromHex(0xff,0xa0,0x00);
  if(m==1.0)return fromHex(0xff,0xec,0xb3);
  if(m==2.0)return fromHex(0xff,0xc1,0x07);
  if(m==3.0)return fromHex(0xff,0x40,0x81);
  if(m==4.0)return fromHex(0x75,0x75,0x75);
  if(m==5.0)return fromHex(0xbd,0xbd,0xbd);
  return vec3(1.0);//dont mind
}

vec3 getFilledJuliaSetColor(int iteration,int colorPattern,int colorPatternIndex,int colorGradation){
  int colorIndex=iteration+int(colorPatternIndex);
  vec3 color=vec3(1.0);
  if(colorPattern==0)color=getColor0(colorIndex);
  if(colorPattern==1)color=getColor1(colorIndex);
  if(colorPattern==2)color=getColor2(colorIndex);
  if(colorPattern==3)color=getColor3(colorIndex);
  
  float cgf=float(colorGradation);
  float light=(cgf-mod(float(iteration/6),cgf))/cgf;
  color=color*light;
  return color;
}

#pragma glslify: export(getFilledJuliaSetColor)