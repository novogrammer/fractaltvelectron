import {
  degToRad,
  circularGetter,
  map,
} from "../../../math_utils.es6";

import PixiRenderPassBase from "./PixiRenderPassBase";
import BeatMaker from "../../BeatMaker";

import FlyweightFactory from "../../FlyweightFactory";
const ROOT_Y=350;
const LENGTH=200;
const COLOR_FROM=0xff0000;
const MAX_DEPTH=7;

export default class PixiRenderPassFractalTree extends PixiRenderPassBase{
  constructor({config}){
    super({config});
    this.beatMaker=new BeatMaker({
      bpm:120,
      timeStart:this.timeNow,
    });
    this.previousBeat=this.beatMaker.getBeat();
    this.resetParams();
    this.setupScene();
  }
  resetParams(){
    this.maxDepth=MAX_DEPTH;
    this.numberOfBranches=2;
    this.branchScale=0.75;
    this.branchAngle=30;
    this.offsetAngle=0;
    this.hue=30;
    this.colorFlyweightFactory=new FlyweightFactory(function({hue,depth}){
      let color=chroma(COLOR_FROM)
      .set("hsl.h",(hue*depth)%360)
      .num();
      return color;
      
    },function(keyA,keyB){
      return keyA.hue==keyB.hue && keyA.depth==keyB.depth;
    });
  }
  setupScene(){
    let nativeLines=false;
    this.g=new PIXI.Graphics(nativeLines);
    this.scene.addChild(this.g);
    this.folder=this.gui.addFolder("PixiRenderPassFractalTree");
    this.folder.open();
    this.setupSliders([
      {
        name:"maxDepth",
        min:0,
        max:MAX_DEPTH,
        step:1,
      },
      {
        name:"numberOfBranches",
        min:1,
        max:3,
        step:1,
      },
      {
        name:"branchScale",
        min:0.1,
        max:1,
        step:0.025,
      },
      {
        name:"branchAngle",
        min:0,
        max:180,
        step:5,
      },
      {
        name:"offsetAngle",
        min:-180,
        max:180,
        step:5,
      },
      {
        name:"hue",
        min:0,
        max:360,
        step:5,
      },
    ]);
  }
  renderBranch(depth=0){
    let halfWay=(this.numberOfBranches-1)/2;
    
    let color=this.colorFlyweightFactory.getValue({
      hue:this.hue,
      depth:depth,
    });
    {
      let p1=new PIXI.Point(0,0);
      p1=this.matrix.apply(p1);
      let p2=new PIXI.Point(0,LENGTH*-1);
      p2=this.matrix.apply(p2);
      this.g.lineStyle(1,color,1,1);
      this.g.moveTo(p1.x,p1.y);
      this.g.lineTo(p2.x,p2.y);
    }
    
    if(depth<this.maxDepth){
      for(let i=0;i<this.numberOfBranches;++i){
        this.pushMatrix();
        let m=new PIXI.Matrix();
        m.scale(this.branchScale,this.branchScale);
        m.rotate(degToRad((this.branchAngle)*(i-halfWay)+this.offsetAngle));
        m.translate(0,LENGTH*-1);
        this.matrix.append(m);
        this.renderBranch(depth+1);
        this.popMatrix();
      }
    }
  }
  update(timeNow){
    this.beatMaker.update(timeNow);
    let beat=this.beatMaker.getBeat();
    if(this.isAutomatic){
      if(beat.count!=this.previousBeat.count){
        //this.hue= Math.round(Math.sin(degToRad(timeNow/(beat.spb*16)*360))*180+180,-1);
        let pattern=[
          30,30,30,30,
          15,30,60,30,
          30,30,30,30,
          15,30,60,90,
        ];
        let step=circularGetter(pattern,beat.count);
        this.branchAngle=step;
      }
    }
    
    this.previousBeat=beat;
  }

  render(){
    let width=this.renderTexture.width;
    let height=this.renderTexture.height;
    this.g.clear();
    //this.g.beginFill(0xffffff);
    this.clearStacks();
    this.pushMatrix();
    let m=new PIXI.Matrix();
    m.translate(width/2,height/2+ROOT_Y);
    this.matrix.append(m);
    this.renderBranch();
    this.popMatrix();
    super.render();
  }
  
}

