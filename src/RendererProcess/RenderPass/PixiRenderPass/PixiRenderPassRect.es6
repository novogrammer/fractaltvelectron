import PixiRenderPassBase from "./PixiRenderPassBase";

export default class PixiRenderPassRect extends PixiRenderPassBase{
  constructor({config}){
    super({config});
    this.setupScene();
  }
  setupScene(){
    this.g=new PIXI.Graphics();
    this.scene.addChild(this.g);
    this.folder=this.gui.addFolder("PixiRenderPassRect");
  }
  render(){
    let {width,height}=this.renderTexture;
    this.g.clear();
    this.g.beginFill(0xffffff);
    this.g.drawRect(0,0,width,height);
    this.g.beginFill(0xff0000);
    this.g.drawRect(width/3*1,height/3*1,width/3*1,height/3*1);
    super.render();
  }
}

