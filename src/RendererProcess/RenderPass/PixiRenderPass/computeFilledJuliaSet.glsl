
#define ITERATION_MAX 1000

vec2 computeFilledJuliaSet(vec2 z0,vec2 c){
  int iteration=0;
  vec2 z=z0;
  bool isEnd=false;
  for(int i=0;i<ITERATION_MAX;++i){
    iteration=i;
    //Z[n+1]=Z[n]^2+C
    vec2 sqZ=vec2(z.x*z.x-z.y*z.y,2.0*z.x*z.y);
    z=sqZ+c;
    if(length(z)>2.0){
      isEnd=true;
      break;
    }
  }
  return vec2(float(iteration),float(isEnd));
  
}
#pragma glslify: export(computeFilledJuliaSet)



