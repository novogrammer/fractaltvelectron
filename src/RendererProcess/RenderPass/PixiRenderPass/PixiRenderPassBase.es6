import RenderPassBase from "../RenderPassBase";

export default class PixiRenderPassBase extends RenderPassBase{
  constructor({config}){
    super({config});
    this.renderer=config.rendererFactory.getPixiRenderer();
    this.renderTexture=PIXI.RenderTexture.create(128,128,PIXI.SCALE_MODES.NEAREST,1.0);
    this.scene=new PIXI.Container();
    this.clearStacks();
  }
  getPixiTexture(){
    return this.renderTexture;
  }
  render(){
    this.renderer.render(this.scene,this.renderTexture,true);
  }
  pushMatrix(){
    this.matrixStack.push(this.matrix.clone());
    this.matrix=this.matrixStack[this.matrixStack.length-1];
  }
  popMatrix(){
    this.matrixStack.pop();
    this.matrix=this.matrixStack[this.matrixStack.length-1];
    if(!this.matrix){
      throw new Error("popMatrix failed");
    }
  }
  clearStacks(){
    this.matrix=new PIXI.Matrix();
    this.matrixStack=[this.matrix];
  }
  
  onResize({width,height}){
    this.renderTexture.resize(width,height);
    super.onResize({width,height})
  }
}
