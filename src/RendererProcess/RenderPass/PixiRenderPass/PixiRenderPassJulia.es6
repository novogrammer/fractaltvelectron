import {
  degToRad,
  circularGetter,
  map,
} from "../../../math_utils.es6";
import {
  PIXI_FRAG_COMMON,
} from "../../../constants.es6";

import PixiRenderPassBase from "./PixiRenderPassBase";
import BeatMaker from "../../BeatMaker";


//for extends
const PIXI=require('pixi.js');

const glsl=require("glslify");


class JuliaFilter extends PIXI.Filter{
  constructor(){
    let uniforms={
      time:{type:'1f',value:0},
      scale:{type:'1f',value:0.5},
      position:{type:'2f',value:[0,0]},
      colorPattern:{type:'1i',value:0},
      colorPatternIndex:{type:'1i',value:0},
      colorGradation:{type:'1i',value:1},
      
    };
    let fragmentShader=glsl(`
${PIXI_FRAG_COMMON}

uniform float time;
uniform float scale;
uniform vec2 position;
uniform int colorPattern;
uniform int colorPatternIndex;
uniform int colorGradation;

#pragma glslify: compute = require("./computeFilledJuliaSet.glsl")
#pragma glslify: getColor = require("./getFilledJuliaSetColor.glsl")

void main(void)
{
  vec2 mappedCoord=mapCoord(vTextureCoord);
  vec2 centeredUv=(mappedCoord-filterArea.xy/2.0)/filterArea.y;
  vec2 c=vec2(0.5);
  vec2 z0=centeredUv/scale+position;

  vec2 result=compute(z0,c);
  int iteration=int(result.x);
  bool isEnd=bool(result.y);
  
  if(isEnd){
    vec3 color=getColor(iteration,colorPattern,colorPatternIndex,colorGradation);
    gl_FragColor=vec4(color,1.0);
  }else{
    discard;
  }

}

`);
    super(null,fragmentShader,uniforms);
    this.padding=0;
  }
  
}



export default class PixiRenderPassJulia extends PixiRenderPassBase{
  constructor({config}){
    super({config});
    this.beatMaker=new BeatMaker({
      bpm:140,
      timeStart:this.timeNow,
    });
    this.previousBeat=this.beatMaker.getBeat();
    this.resetParams();
    this.setupScene();
  }
  resetParams(){
    this.positionX=-0.75;
    this.positionY=0;
    this.scaleFactor=-0.3;//log10(0.5)=-0.30102999566
    this.colorPattern=0;
    this.colorPatternIndex=0;
    this.colorGradation=1;
  }
  setupScene(){
    this.g=new PIXI.Graphics();
    this.scene.addChild(this.g);
    this.juliaFilter=new JuliaFilter();
    this.g.filters=[this.juliaFilter];
    
    this.folder=this.gui.addFolder("PixiRenderPassJulia");
    this.folder.open();
    this.setupSliders([
      {
        name:"positionX",
        min:-1.5,
        max:1.5,
        step:0.001,
      },
      {
        name:"positionY",
        min:-1.5,
        max:1.5,
        step:0.001,
      },
      {
        name:"scaleFactor",
        min:-1,
        max:4,
        step:0.1,
      },
      {
        name:"colorPattern",
        min:0,
        max:3,
        step:1,
      },
      {
        name:"colorPatternIndex",
        min:0,
        max:5,
        step:1,
      },
      {
        name:"colorGradation",
        min:1,
        max:10,
        step:1,
      },
    ]);
  }
  getScale(){
    return Math.pow(10,this.scaleFactor);
  }
  update(timeNow){
    this.beatMaker.update(timeNow);
    let beat=this.beatMaker.getBeat();
    if(this.isAutomatic){
      let scaleAngle=degToRad(timeNow/(beat.spb*64)*360);
      this.scaleFactor=map(Math.cos(scaleAngle),-1,1,-1,2.5);
      this.positionX=0.10692519971872998;
      this.positionY=0.6360101377585149;
      if(beat.count!=this.previousBeat.count){
        this.colorPatternIndex=Math.floor(beat.count/2)%6;
      }
    }
    
    this.previousBeat=beat;
  }
  
  render(){
    let {width,height}=this.renderTexture;
    this.g.clear();
    this.g.beginFill(0xffffff);
    this.g.drawRect(0,0,width,height);
    this.juliaFilter.uniforms.time=this.timeNow;
    this.juliaFilter.uniforms.scale=this.getScale();
    this.juliaFilter.uniforms.position=[this.positionX,this.positionY];
    
    this.juliaFilter.uniforms.colorPattern=this.colorPattern;
    this.juliaFilter.uniforms.colorPatternIndex=this.colorPatternIndex;
    this.juliaFilter.uniforms.colorGradation=this.colorGradation;
    super.render();
  }
  setParamByRelative(index,paramByRelative){
    if(!this.sliderDefinitions){
      //throw new Error("!this.sliderDefinitions");
      console.log("!this.sliderDefinitions");
      return;
    }
    let sliderDefinition=this.sliderDefinitions[index];
    if(!sliderDefinition){
      //throw new Error("!sliderDefinition");
      console.log("!sliderDefinition");
      return;
    }
    let {name,min,max,step}=sliderDefinition;
    let slider=this.getSlider(name)
    if(!slider){
      throw new Error("!slider");
    }
    
    if(name=="positionX"||name=="positionY"){
      //scaleに応じた独自処理を行う
      let scale=this.getScale();
      this[name]+=paramByRelative/scale*0.1;
      if(max<this[name]){
        //Keep it simple, stupid.
        this[name]=min;
      }
      if(this[name]<min){
        //Keep it simple, stupid.
        this[name]=max;
      }
    }else{
      super.setParamByRelative(index,paramByRelative);
    }
  }
  
}

