import ThreeRenderPassBase from "./ThreeRenderPassBase";
import BeatMaker from "../../BeatMaker";

import {circularGetter} from "../../../math_utils";

export default class ThreeRenderPassIntroduction extends ThreeRenderPassBase{
  constructor({config}){
    super({config});
    this.beatMaker=new BeatMaker({
      bpm:120,
      timeStart:this.timeNow,
    });
    this.previousBeat=this.beatMaker.getBeat();
    this.setupScene();
  }
  setupScene(){
    let geometry=new THREE.BoxGeometry(1,1,1);
    let material=new THREE.MeshBasicMaterial({color:0x00ff00});
    this.cube=new THREE.Mesh(geometry,material);
    this.scene.add(this.cube);
    this.camera.position.z=5;
    
    this.folder=this.gui.addFolder("ThreeRenderPassIntroduction");
    this.folder.open();
    this.setupSliders([
      {
        name:"rx",
        min:-Math.PI,
        max:+Math.PI,
        step:0.1,
      },
      {
        name:"ry",
        min:-Math.PI,
        max:+Math.PI,
        step:0.1,
      },
      {
        name:"rz",
        min:-Math.PI,
        max:+Math.PI,
        step:0.1,
      },
    ]);
  }
  update(timeNow){
    this.beatMaker.update(timeNow);
    let beat=this.beatMaker.getBeat();
    if(this.isAutomatic){
      if(beat.count!=this.previousBeat.count){
        let pattern=[0,1,0,-1,1,-2,2,-1];
        let step=circularGetter(pattern,beat.count);
        if(step!=0){
          let target=this.cube.rotation.y+step;
          let tween=new TWEEN.Tween(this.cube.rotation);
          tween
          .to({y:target},beat.spb/2*1000)
          .easing(TWEEN.Easing.Elastic.Out)
          .start();
        }
      }
    }
    
    this.previousBeat=beat;
  }
  render(){
    super.render();
  }
  get rx(){
    return this.cube.rotation.x;
  }
  set rx(rx){
    this.cube.rotation.x=rx;
  }
  get ry(){
    return this.cube.rotation.y;
  }
  set ry(ry){
    this.cube.rotation.y=ry;
  }
  get rz(){
    return this.cube.rotation.z;
  }
  set rz(rz){
    this.cube.rotation.z=rz;
  }

}