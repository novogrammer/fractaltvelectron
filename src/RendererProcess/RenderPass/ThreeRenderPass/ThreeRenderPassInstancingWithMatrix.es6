import ThreeRenderPassBase from "./ThreeRenderPassBase";


let vertexShader=`
precision highp float;
attribute vec3 mcol0;
attribute vec3 mcol1;
attribute vec3 mcol2;
attribute vec3 mcol3;
varying vec3 vColor;
void main(){
  vColor = color;
  mat4 instanceMatrix = mat4(
    vec4( mcol0, 0 ),
    vec4( mcol1, 0 ),
    vec4( mcol2, 0 ),
    vec4( mcol3, 1 )
  );
  vec3 transformed = (instanceMatrix * vec4(position,1.0)).xyz;
  gl_Position = projectionMatrix * modelViewMatrix * vec4( transformed, 1.0 );
}
`;
let fragmentShader=`
precision highp float;
varying vec3 vColor;
void main() {
  gl_FragColor = vec4( vColor, 1.0 );
}
`;



export default class ThreeRenderPassInstancingWithMatrix extends ThreeRenderPassBase{
  constructor({config}){
    super({config});
    this.setupScene();
  }
  setupScene(){
    let geometry=new THREE.IcosahedronBufferGeometry( 0.1, 1 );
    let colors=[];
    for(let i=0,l=geometry.attributes.position.count;i<l;++i){
      colors.push(Math.random(),Math.random(),Math.random());
    }
    geometry.addAttribute('color',new THREE.Float32BufferAttribute(colors,3));
    
    let material=new THREE.MeshBasicMaterial({color:0xff0000,vertexColors:THREE.VertexColors});
    
    let instances=1000;
    
    let mcol0Array=[];
    let mcol1Array=[];
    let mcol2Array=[];
    let mcol3Array=[];
    for(let i=0;i<instances;++i){
      let mesh=new THREE.Mesh(geometry,material);
      this.scene.add(mesh);
      
      let position=mesh.position;
      let quaternion=mesh.quaternion;
      let scale=mesh.scale;
      
      position.set(Math.random()*2-1,Math.random()*2-1,Math.random()*2-1);
      quaternion.set(Math.random()*2-1,Math.random()*2-1,Math.random()*2-1,Math.random()*2-1);
      quaternion.normalize();
      scale.set(Math.random()*2,Math.random()*2,Math.random()*2);
      
      mesh.updateMatrix();
      let me=mesh.matrix.elements;
      mcol0Array.push(me[0],me[1],me[2]);
      mcol1Array.push(me[4],me[5],me[6]);
      mcol2Array.push(me[8],me[9],me[10]);
      mcol3Array.push(me[12],me[13],me[14]);
    }
    
    let instancedGeometry=new THREE.InstancedBufferGeometry();
    instancedGeometry.attributes.position=geometry.attributes.position;
    instancedGeometry.attributes.color=geometry.attributes.color;
    
    instancedGeometry.addAttribute("mcol0",new THREE.InstancedBufferAttribute(new Float32Array(mcol0Array),3));
    instancedGeometry.addAttribute("mcol1",new THREE.InstancedBufferAttribute(new Float32Array(mcol1Array),3));
    instancedGeometry.addAttribute("mcol2",new THREE.InstancedBufferAttribute(new Float32Array(mcol2Array),3));
    instancedGeometry.addAttribute("mcol3",new THREE.InstancedBufferAttribute(new Float32Array(mcol3Array),3));
    
    let shaderMaterial=new THREE.ShaderMaterial({
      uniforms:{},
      vertexShader:vertexShader,
      fragmentShader:fragmentShader,
      vertexColors:true,
    });
    
    let instancedMesh=new THREE.Mesh(instancedGeometry,shaderMaterial);
    instancedMesh.position.x=0.1;
    this.scene.add(instancedMesh);
    
    this.camera.position.z=5;
    
    this.folder=this.gui.addFolder("ThreeRenderPassInstancingWithMatrix");
    this.folder.open();

  }
  render(){
    super.render();
  }


}