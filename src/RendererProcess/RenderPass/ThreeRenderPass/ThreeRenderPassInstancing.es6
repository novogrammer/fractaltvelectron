import ThreeRenderPassBase from "./ThreeRenderPassBase";


let vertexShader=`
precision highp float;
attribute vec3 instancePosition;
attribute vec4 instanceQuaternion;
attribute vec3 instanceScale;
varying vec3 vColor;
vec3 applyTRS( vec3 position, vec3 translation, vec4 quaternion, vec3 scale ) {
  position *= scale;
  position += 2.0 * cross( quaternion.xyz, cross( quaternion.xyz, position ) + quaternion.w * position );
  return position + translation;
}
void main(){
  vColor = color;
  vec3 transformed = applyTRS( position.xyz, instancePosition, instanceQuaternion, instanceScale );
  gl_Position = projectionMatrix * modelViewMatrix * vec4( transformed, 1.0 );
}
`;
let fragmentShader=`
precision highp float;
varying vec3 vColor;
void main() {
  gl_FragColor = vec4( vColor, 1.0 );
}
`;



export default class ThreeRenderPassInstancing extends ThreeRenderPassBase{
  constructor({config}){
    super({config});
    this.setupScene();
  }
  setupScene(){
    let geometry=new THREE.IcosahedronBufferGeometry( 0.1, 1 );
    let colors=[];
    for(let i=0,l=geometry.attributes.position.count;i<l;++i){
      colors.push(Math.random(),Math.random(),Math.random());
    }
    geometry.addAttribute('color',new THREE.Float32BufferAttribute(colors,3));
    
    let material=new THREE.MeshBasicMaterial({color:0xff0000,vertexColors:THREE.VertexColors});
    
    let instances=1000;
    
    let instancePositions=[];
    let instanceQuaternions=[];
    let instanceScales=[];
    for(let i=0;i<instances;++i){
      let mesh=new THREE.Mesh(geometry,material);
      this.scene.add(mesh);
      
      let position=mesh.position;
      let quaternion=mesh.quaternion;
      let scale=mesh.scale;
      
      position.set(Math.random()*2-1,Math.random()*2-1,Math.random()*2-1);
      quaternion.set(Math.random()*2-1,Math.random()*2-1,Math.random()*2-1,Math.random()*2-1);
      quaternion.normalize();
      scale.set(Math.random()*2,Math.random()*2,Math.random()*2);
      
      instancePositions.push(position.x,position.y,position.z);
      instanceQuaternions.push(quaternion.x,quaternion.y,quaternion.z,quaternion.w);
      instanceScales.push(scale.x,scale.y,scale.z);
    }
    
    let instancedGeometry=new THREE.InstancedBufferGeometry();
    instancedGeometry.attributes.position=geometry.attributes.position;
    instancedGeometry.attributes.color=geometry.attributes.color;
    
    instancedGeometry.addAttribute("instancePosition",new THREE.InstancedBufferAttribute(new Float32Array(instancePositions),3));
    instancedGeometry.addAttribute("instanceQuaternion",new THREE.InstancedBufferAttribute(new Float32Array(instanceQuaternions),4));
    instancedGeometry.addAttribute("instanceScale",new THREE.InstancedBufferAttribute(new Float32Array(instanceScales),3));
    
    let shaderMaterial=new THREE.ShaderMaterial({
      uniforms:{},
      vertexShader:vertexShader,
      fragmentShader:fragmentShader,
      vertexColors:true,
    });
    
    let instancedMesh=new THREE.Mesh(instancedGeometry,shaderMaterial);
    instancedMesh.position.x=0.1;
    this.scene.add(instancedMesh);
    
    this.camera.position.z=5;
    
    this.folder=this.gui.addFolder("ThreeRenderPassInstancing");
    this.folder.open();

  }
  render(){
    super.render();
  }


}