import RenderPassBase from "../RenderPassBase";


export default class ThreeRenderPassBase extends RenderPassBase{
  constructor({config}){
    super({config});
    //canvas経由でやりとりするので、RenderPassごとにcanvasへ書き込む
    this.renderer=config.rendererFactory.getThreeRenderer();
    this.scene=new THREE.Scene();
    this.camera=new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
    this.pixiTexture=PIXI.Texture.from(this.renderer.domElement);
    this.clearStacks();
  }
  getPixiTexture(){
    this.pixiTexture.update();
    return this.pixiTexture;
  }
  render(){
    this.renderer.render(this.scene,this.camera);
  }
  pushMatrix(){
    this.matrixStack.push(this.matrix.clone());
    this.matrix=this.matrixStack[this.matrixStack.length-1];
  }
  popMatrix(){
    this.matrixStack.pop();
    this.matrix=this.matrixStack[this.matrixStack.length-1];
    if(!this.matrix){
      throw new Error("popMatrix failed");
    }
  }
  clearStacks(){
    this.matrix=new THREE.Matrix4();
    this.matrixStack=[this.matrix];
  }

  onResize({width,height}){
    this.renderer.setSize(width,height);
    super.onResize({width,height});
  }
}