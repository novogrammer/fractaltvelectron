import {
  degToRad,
  circularGetter,
} from "../../../math_utils.es6";


import ThreeRenderPassBase from "./ThreeRenderPassBase";
import BeatMaker from "../../BeatMaker";

import FlyweightFactory from "../../FlyweightFactory";
const LENGTH=1;
const COLOR_FROM=0xff0000;


let vertexShader=`
precision highp float;
attribute vec3 mcol0;
attribute vec3 mcol1;
attribute vec3 mcol2;
attribute vec3 mcol3;
attribute vec3 instanceColor;
attribute float instanceVisible;
varying vec3 vColor;
varying float vInstanceVisible;
void main(){
  vColor = instanceColor;
  vInstanceVisible=instanceVisible;
  mat4 instanceMatrix = mat4(
    vec4( mcol0, 0 ),
    vec4( mcol1, 0 ),
    vec4( mcol2, 0 ),
    vec4( mcol3, 1 )
  );
  vec3 transformed = (instanceMatrix * vec4(position,1.0)).xyz;
  gl_Position = projectionMatrix * modelViewMatrix * vec4( transformed, 1.0 );
}
`;
let fragmentShader=`
precision highp float;
varying vec3 vColor;
varying float vInstanceVisible;
void main() {
  if(0.0<vInstanceVisible){
    gl_FragColor = vec4( vColor, 1.0 );
  }else{
    discard;
  }
}
`;

//const INSTANCES=10000;
const MAX_BRANCH=2;
const MAX_DEPTH=10;
const INSTANCES=Math.pow(MAX_BRANCH,MAX_DEPTH+1);


export default class ThreeRenderPassFractalTree extends ThreeRenderPassBase{
  constructor({config}){
    super({config});
    this.currentInstance=0;
    this.beatMaker=new BeatMaker({
      bpm:140,
      timeStart:this.timeNow,
    });
    this.previousBeat=this.beatMaker.getBeat();
    
    this.temporary={
      position:new THREE.Vector3(0,LENGTH/2,0),
      euler:new THREE.Euler( 0, 0, 0, 'XYZ' ),
      quaternion:new THREE.Quaternion(),
      scale:new THREE.Vector3(1,1,1),
      matrix:new THREE.Matrix4(),
    }
    
    this.resetParams();
    this.setupScene();
  }
  resetParams(){
    this.maxDepth=MAX_DEPTH;
    this.numberOfBranches=2;
    this.branchScale=0.75;
    this.branchAngleZ=30;
    this.branchAngleY=0;
    this.offsetAngle=0;
    this.cameraAngleY=0;
    this.hue=30;
    this.colorFlyweightFactory=new FlyweightFactory(function({hue,depth}){
      let color=chroma(COLOR_FROM)
      .set("hsl.h",(hue*depth)%360)
      .darken(depth/MAX_DEPTH*2)
      .rgb();
      return color;
      
    },function(keyA,keyB){
      return keyA.hue==keyB.hue && keyA.depth==keyB.depth;
    });
  }
  setupScene(){
    let geometry=new THREE.CylinderBufferGeometry(LENGTH*0.3,LENGTH*0.3,LENGTH);
    
    let instancedGeometry=new THREE.InstancedBufferGeometry();
    instancedGeometry.index = geometry.index;
    instancedGeometry.attributes.position=geometry.attributes.position;
    
    this.bufferAttributeMcol0=new THREE.InstancedBufferAttribute(new Float32Array(INSTANCES*3),3);
    this.bufferAttributeMcol1=new THREE.InstancedBufferAttribute(new Float32Array(INSTANCES*3),3);
    this.bufferAttributeMcol2=new THREE.InstancedBufferAttribute(new Float32Array(INSTANCES*3),3);
    this.bufferAttributeMcol3=new THREE.InstancedBufferAttribute(new Float32Array(INSTANCES*3),3);
    this.bufferAttributeInstanceColor=new THREE.InstancedBufferAttribute(new Float32Array(INSTANCES*3),3);
    this.bufferAttributeInstanceVisible=new THREE.InstancedBufferAttribute(new Float32Array(INSTANCES*1),1);
    
    instancedGeometry.addAttribute("mcol0",this.bufferAttributeMcol0);
    instancedGeometry.addAttribute("mcol1",this.bufferAttributeMcol1);
    instancedGeometry.addAttribute("mcol2",this.bufferAttributeMcol2);
    instancedGeometry.addAttribute("mcol3",this.bufferAttributeMcol3);
    instancedGeometry.addAttribute("instanceColor",this.bufferAttributeInstanceColor);
    instancedGeometry.addAttribute("instanceVisible",this.bufferAttributeInstanceVisible);
    
    let shaderMaterial=new THREE.ShaderMaterial({
      uniforms:{},
      vertexShader:vertexShader,
      fragmentShader:fragmentShader,
      blending:THREE.AdditiveBlending,
      transparent:true,
      depthWrite:false,
    });
    
    let instancedMesh=new THREE.Mesh(instancedGeometry,shaderMaterial);
    instancedMesh.position.x=0.1;
    this.scene.add(instancedMesh);
    
    this.gridHelper=new THREE.GridHelper( 10, 10 );
    this.scene.add(this.gridHelper);
    
    this.folder=this.gui.addFolder("ThreeRenderPassFractalTree");
    this.folder.open();
    this.setupSliders([
      {
        name:"maxDepth",
        min:0,
        max:MAX_DEPTH,
        step:1,
      },
      {
        name:"branchScale",
        min:0.5,
        max:1,
        step:0.025,
      },
      {
        name:"branchAngleZ",
        min:0,
        max:180,
        step:5,
      },
      {
        name:"offsetAngle",
        min:-180,
        max:180,
        step:5,
      },
      {
        name:"branchAngleY",
        min:-180,
        max:180,
        step:5,
      },
      {
        name:"cameraAngleY",
        min:-180,
        max:180,
        step:5,
      },
      {
        name:"numberOfBranches",
        min:1,
        max:MAX_BRANCH,
        step:1,
      },
      {
        name:"hue",
        min:0,
        max:360,
        step:5,
      },
    ]);

  }
  update(timeNow){
    this.beatMaker.update(timeNow);
    let beat=this.beatMaker.getBeat();
    if(this.isAutomatic){
      this.cameraAngleY=timeNow/(beat.spb*64)*360;
      if(beat.count!=this.previousBeat.count){
        //this.hue= Math.round(Math.sin(degToRad(timeNow/(beat.spb*16)*360))*180+180,-1);
        let pattern=[
          30,30,30,30,
          15,30,60,30,
          30,30,30,30,
          15,30,60,90,
        ];
        let step=circularGetter(pattern,beat.count);
        this.branchAngleZ=step;
      }
    }
    
    this.previousBeat=beat;
  }
  resetInstance(){
    for(let i=0;i<INSTANCES;++i){
      this.bufferAttributeMcol0.setXYZ(i,1,0,0);
      this.bufferAttributeMcol1.setXYZ(i,0,1,0);
      this.bufferAttributeMcol2.setXYZ(i,0,0,1);
      this.bufferAttributeMcol3.setXYZ(i,0,0,0);
      this.bufferAttributeInstanceColor.setXYZ(i,1,0,0);
      this.bufferAttributeInstanceVisible.setX(i,0);
    }
    this.bufferAttributeMcol0.needsUpdate=true;
    this.bufferAttributeMcol1.needsUpdate=true;
    this.bufferAttributeMcol2.needsUpdate=true;
    this.bufferAttributeMcol3.needsUpdate=true;
    this.bufferAttributeInstanceColor.needsUpdate=true;
    this.bufferAttributeInstanceVisible.needsUpdate=true;
    this.currentInstance=0;
    
  }
  addInstance(matrix,color){
    if(!(this.currentInstance<INSTANCES)){
      console.log("addInstance failed.");
      return;
    }
    
    let me=matrix.elements;
    this.bufferAttributeMcol0.setXYZ(this.currentInstance,me[0],me[1],me[2]);
    this.bufferAttributeMcol1.setXYZ(this.currentInstance,me[4],me[5],me[6]);
    this.bufferAttributeMcol2.setXYZ(this.currentInstance,me[8],me[9],me[10]);
    this.bufferAttributeMcol3.setXYZ(this.currentInstance,me[12],me[13],me[14]);
    this.bufferAttributeInstanceColor.setXYZ(this.currentInstance,color[0]/255,color[1]/255,color[2]/255);
    
    this.bufferAttributeInstanceVisible.setX(this.currentInstance,1);
    
    ++this.currentInstance;
  }
  renderBranch(depth=0){
    let halfWay=(this.numberOfBranches-1)/2;
    let color=this.colorFlyweightFactory.getValue({
      hue:this.hue,
      depth:depth,
    });
    this.pushMatrix();
    
    
    this.addInstance(this.matrix,color);
    
    if(depth<this.maxDepth){
      for(let i=0;i<this.numberOfBranches;++i){
        this.pushMatrix();
        this.temporary.matrix.makeTranslation(this.temporary.position.x,this.temporary.position.y,this.temporary.position.z);
        this.matrix.multiplyMatrices( this.matrix, this.temporary.matrix );
        
        this.temporary.euler.y=degToRad(this.branchAngleY);
        this.temporary.euler.z=degToRad((this.branchAngleZ)*(i-halfWay)+this.offsetAngle);
        this.temporary.quaternion.setFromEuler(this.temporary.euler);
        let scale=new THREE.Vector3(this.branchScale,this.branchScale,this.branchScale);
        this.temporary.scale.set(this.branchScale,this.branchScale,this.branchScale);
        this.temporary.matrix.compose({x:0,y:0,z:0},this.temporary.quaternion,this.temporary.scale);
        
        this.matrix.multiplyMatrices( this.matrix, this.temporary.matrix );
        
        this.temporary.matrix.makeTranslation(this.temporary.position.x,this.temporary.position.y,this.temporary.position.z);
        this.matrix.multiplyMatrices( this.matrix, this.temporary.matrix );
        this.renderBranch(depth+1);
        this.popMatrix();
      }
    }
    this.popMatrix();
  }
  render(){
    this.resetInstance();
    this.clearStacks();
    this.pushMatrix();
    let m=new THREE.Matrix4();
    m.makeTranslation(0,LENGTH*0.5,0);
    this.matrix.multiplyMatrices( this.matrix, m );
    this.renderBranch();
    this.popMatrix();
    //console.log(this.currentInstance);
    
    this.camera.position.set(
      Math.sin(degToRad(this.cameraAngleY))*5,
      3,
      Math.cos(degToRad(this.cameraAngleY))*5
    );
    this.camera.lookAt(0,2,0);
    
    super.render();
  }


}