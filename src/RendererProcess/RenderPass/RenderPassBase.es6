
import BeatMaker from "../BeatMaker";
export default class RenderPassBase{
  constructor({config}){
    let {emitter,gui,timeNow}=config;
    this.emitter=emitter;
    this.timeNow=timeNow;
    this.emitter.on("resize",this.onResize,this);
    this.gui=gui;
    this.folder=null;
    this.matrix=null;
    this.matrixStacks=null;
    this.isAutomatic=false;
    
  }
  onResize({width,height}){
  }
  getPixiTexture(){
    return nulll;
  }
  update(timeNow){
    this.timeNow=timeNow;
  }
  render(){
    //DO NOTHING
  }
  hideGui(){
    if(this.folder){
      this.folder.domElement.style.display='none';
    }
  }
  showGui(){
    if(this.folder){
      this.folder.domElement.style.display='';
    }
  }
  setupSliders(sliderDefinitions){
    for(let sliderDefinition of sliderDefinitions){
      let sliderName=this.makeSliderName(sliderDefinition.name);
      this[sliderName]=this.folder.add(
        this,
        sliderDefinition.name,
        sliderDefinition.min,
        sliderDefinition.max,
        sliderDefinition.step).listen();
    }
    this.sliderDefinitions=sliderDefinitions;
  }
  setParamByRelative(index,paramByRelative){
    if(!this.sliderDefinitions){
      //throw new Error("!this.sliderDefinitions");
      console.log("!this.sliderDefinitions");
      return;
    }
    let sliderDefinition=this.sliderDefinitions[index];
    if(!sliderDefinition){
      //throw new Error("!sliderDefinition");
      console.log("!sliderDefinition");
      return;
    }
    let {name,min,max,step}=sliderDefinition;
    let slider=this.getSlider(name)
    if(!slider){
      throw new Error("!slider");
    }
    this[name]+=step*paramByRelative;
    if(max<this[name]){
      //Keep it simple, stupid.
      this[name]=min;
    }
    if(this[name]<min){
      //Keep it simple, stupid.
      this[name]=max;
    }
  }
  getSlider(name){
    let sliderName=this.makeSliderName(name);
    return this[sliderName];
  }
  makeSliderName(name){
    return name+"Slider";
  }
  resetParams(){
    //DO NOTHING
  }
  pushMatrix(){
    //DO NOTHING
  }
  popMatrix(){
    //DO NOTHING
  }
  clearStacks(){
    //DO NOTHING
  }
  setAutomatic(isAutomatic){
    this.isAutomatic=!!isAutomatic;
  }
}

