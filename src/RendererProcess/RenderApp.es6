import {
  IS_DEBUG,
  FPS,
  RESET_TIME,
} from "../constants";

const electron=require("electron");
const {remote,ipcRenderer}=electron;
global.THREE=require('three');
//require('../node_modules/three/examples/js/loaders/OBJLoader.js');
global.PIXI=require('pixi.js');
global.chroma=require('chroma-js');
global.TWEEN=require('@tweenjs/tween.js');
import animate from "animate";
import EventEmitter from "eventemitter3";
import Stats from "stats.js";
import dat from "dat.gui";


import PixiRenderPassNoise from "./RenderPass/PixiRenderPass/PixiRenderPassNoise";
import PixiRenderPassColorBar from "./RenderPass/PixiRenderPass/PixiRenderPassColorBar";
import PixiRenderPassMengerSponge from "./RenderPass/PixiRenderPass/PixiRenderPassMengerSponge";
import PixiRenderPassJulia from "./RenderPass/PixiRenderPass/PixiRenderPassJulia";
import PixiRenderPassMandelbrot from "./RenderPass/PixiRenderPass/PixiRenderPassMandelbrot";
import PixiRenderPassFractalTree from "./RenderPass/PixiRenderPass/PixiRenderPassFractalTree";
import PixiRenderPassSierpinskiGasket from "./RenderPass/PixiRenderPass/PixiRenderPassSierpinskiGasket";
//import PixiRenderPassRect from "./RenderPass/PixiRenderPass/PixiRenderPassRect";

import ThreeRenderPassFractalTree from "./RenderPass/ThreeRenderPass/ThreeRenderPassFractalTree";
//import ThreeRenderPassInstancingWithMatrix from "./RenderPass/ThreeRenderPass/ThreeRenderPassInstancingWithMatrix";
//import ThreeRenderPassInstancing from "./RenderPass/ThreeRenderPass/ThreeRenderPassInstancing";
//import ThreeRenderPassIntroduction from "./RenderPass/ThreeRenderPass/ThreeRenderPassIntroduction";

import PowerOnFilter from "./Filter/PowerOnFilter";
import PowerOffFilter from "./Filter/PowerOffFilter";
import ChannelGlitchFilter from "./Filter/ChannelGlitchFilter";


class RendererFactory{
  constructor(pixiRenderer){
    //インスタンスを使い回すならここで初期化する
    this.threeRenderer=new THREE.WebGLRenderer();
    this.pixiRenderer=pixiRenderer;
  }
  getThreeRenderer(){
    return this.threeRenderer;
  }
  getPixiRenderer(){
    return this.pixiRenderer;
  }
}

export default class RenderApp{
  constructor(){
    this.electron=electron;
    this.ipcRenderer=this.electron.ipcRenderer;
    this.emitter=new EventEmitter();
    this.renderPassIndex=0;
    this.renderPasses=[];
    this.isPowerOn=false;
    this.isAutomatic=false;
    this.isDebug=IS_DEBUG;
    this.powerOffTime=this.getTime();
    this.setupStats();
    this.setupDatGui();
    this.setupRenderer();
    this.setupScene();
    this.setupEvents();
    this.debug=this.isDebug;//set again
  }
  setupStats(){
    this.stats=new Stats();
    let $dom=$(this.stats.dom);
    $dom.attr("id","Stats");
    $("body").append($dom);
    $("#Stats").css({left:0,right:"auto"});
  }
  setupDatGui(){
    this.gui = new dat.GUI({hideable:false});
    this.renderPassIndexController=this.gui.add(this,"renderPassIndex",0,0,1).listen();
    if(!this.debug){
      this.gui.domElement.style.display='none';
    }
  }
  setupRenderer(){
    this.pixiRenderer=new PIXI.WebGLRenderer({
      view:$("#View")[0],
      transparent:false,
      resolution:1,
      antialias:false,
    });
    this.rendererFactory=new RendererFactory(this.pixiRenderer);
    this.sprite=new PIXI.Sprite();
    
  }
  setupScene(){
    let config={
      rendererFactory:this.rendererFactory,
      gui:this.gui,
      emitter:this.emitter,
      timeNow:this.getTime(),
    };
    
    //1ch
    //SUN?
    this.pixiRenderPassColorBar1ch=new PixiRenderPassColorBar({config});
    this.renderPasses.push(this.pixiRenderPassColorBar1ch);
    
    //2ch
    //NHK?
    this.pixiRenderPassMandelbrot=new PixiRenderPassMandelbrot({config});
    this.renderPasses.push(this.pixiRenderPassMandelbrot);
    
    //3ch
    this.pixiRenderPassNoise3ch=new PixiRenderPassNoise({config});
    this.renderPasses.push(this.pixiRenderPassNoise3ch);

    //4ch
    //MBS?
    this.pixiRenderPassFractalTree=new PixiRenderPassFractalTree({config});
    this.renderPasses.push(this.pixiRenderPassFractalTree);
    
    //5ch
    //TVO?
    this.pixiRenderPassColorBar5ch=new PixiRenderPassColorBar({config});
    this.renderPasses.push(this.pixiRenderPassColorBar5ch);
    
    //6ch
    //ABC?
    this.pixiRenderPassSierpinskiGasket=new PixiRenderPassSierpinskiGasket({config});
    this.renderPasses.push(this.pixiRenderPassSierpinskiGasket);
    
    //7ch
    this.pixiRenderPassNoise7ch=new PixiRenderPassNoise({config});
    this.renderPasses.push(this.pixiRenderPassNoise7ch);
    
    //8ch
    //KTV?
    this.pixiRenderPassMengerSponge=new PixiRenderPassMengerSponge({config});
    this.renderPasses.push(this.pixiRenderPassMengerSponge);
    
    //9ch
    this.pixiRenderPassNoise9ch=new PixiRenderPassNoise({config});
    this.renderPasses.push(this.pixiRenderPassNoise9ch);
    
    //10ch
    //YTV?
    this.threeRenderPassFractalTree=new ThreeRenderPassFractalTree({config});
    this.renderPasses.push(this.threeRenderPassFractalTree);
    
    //11ch
    //KBS?
    this.pixiRenderPassColorBar11ch=new PixiRenderPassColorBar({config});
    this.renderPasses.push(this.pixiRenderPassColorBar11ch);
    
    //12ch
    //ETV?
    this.pixiRenderPassJulia=new PixiRenderPassJulia({config});
    this.renderPasses.push(this.pixiRenderPassJulia);
    
    //以下蛇足
    
    //this.threeRenderPassInstancingWithMatrix=new ThreeRenderPassInstancingWithMatrix({config});
    //this.renderPasses.push(this.threeRenderPassInstancingWithMatrix);

    //this.threeRenderPassInstancing=new ThreeRenderPassInstancing({config});
    //this.renderPasses.push(this.threeRenderPassInstancing);
    
    //this.pixiRenderPassRect=new PixiRenderPassRect({config});
    //this.renderPasses.push(this.pixiRenderPassRect);

    //this.threeRenderPassIntroduction=new ThreeRenderPassIntroduction({config});
    //this.renderPasses.push(this.threeRenderPassIntroduction);
    
    this.renderPassIndexController.max(this.renderPasses.length-1);
    this.renderPassIndexController.updateDisplay();
    this.powerOnFilter=new PowerOnFilter();
    
    this.powerOffFilter=new PowerOffFilter();
    
    this.channelGlitchFilter=new ChannelGlitchFilter();
    
    this.sprite.filters=[
      this.channelGlitchFilter,
      this.powerOnFilter,
      this.powerOffFilter,
    ];
    
  }
  setupEvents(){
    this.emitter.on("resize",this.onResize,this);
    this.emitter.on("tick",this.onTick,this);
    ipcRenderer.on("renderPassIndexByRelative",(e,indexByRelative)=>{
      this.onSetRenderPassIndexByRelative(e,indexByRelative);
    });
    ipcRenderer.on("paramByRelative",(e,index,paramByRelative)=>{
      this.onSetParamByRelative(e,index,paramByRelative);
    })
    ipcRenderer.on("switchChanged",(e,index,value)=>{
      this.onSwitchChanged(e,index,value);
    })
    
    $(window).on("resize",(e)=>this.emitResize(e));
    this.emitResize();
    $(window).on("keydown",(e)=>this.onKeyDown(e));

    
    this.animation=animate(()=>{
      this.stats.begin();
      this.emitter.emit("tick");
      this.stats.end();
    },FPS);
    
  }
  getTime(){
    return performance.now()/1000;
  }
  resetParams(){
    if(IS_DEBUG){
      console.log("resetParams");
    }
    this.renderPassIndex=0;
    for(let renderPass of this.renderPasses){
      renderPass.resetParams();
    }
  }
  update(){
    let timeNow=this.getTime();
    for(let renderPass of this.renderPasses){
      renderPass.update(timeNow);
    }
    
  }
  render(){
    
    for(let i=0;i<this.renderPasses.length;++i){
      let renderPass=this.renderPasses[i];
      if(i==this.renderPassIndex){
        renderPass.render();
        this.sprite.texture=renderPass.getPixiTexture();
        renderPass.showGui();
      }else{
        renderPass.hideGui();
      }
    }
    this.pixiRenderer.render(this.sprite);
  }
  emitResize(){
    this.emitter.emit("resize",{
      width:window.innerWidth,
      height:window.innerHeight,
    });
  }
  nextRenderPass(){
    this.setRenderPassIndexByRelative(+1);
  }
  previousRenderPass(){
    this.setRenderPassIndexByRelative(-1);
  }
  setRenderPassIndexByRelative(indexByRelative){
    this.renderPassIndex=Math.floor(this.renderPassIndex+indexByRelative+this.renderPasses.length)%this.renderPasses.length;
    this.channelGlitchFilter.start();
    
  }
  setParamByRelative(index,paramByRelative){
    
    if(this.renderPassIndex<this.renderPasses.length){
      let renderPass=this.renderPasses[this.renderPassIndex];
      renderPass.setParamByRelative(index,paramByRelative);
    }
  }
  set power(isPowerOn){
    this.isPowerOn=!!isPowerOn;
    if(this.isPowerOn){
      this.powerOffFilter.stop();
      this.powerOnFilter.start();
      let now=this.getTime();
      if(RESET_TIME<now-this.powerOffTime){
        this.resetParams();
      }
    }else{
      this.powerOnFilter.stop();
      this.powerOffFilter.start();
      this.powerOffTime=this.getTime();
    }
  }
  get power(){
    return this.isPowerOn;
  }
  set automatic(isAutomatic){
    this.isAutomatic=!!isAutomatic;
    for(let renderPass of this.renderPasses){
      renderPass.setAutomatic(this.isAutomatic);
    }
    
  }
  get automatic(){
    return this.isAutomatic;
  }
  set debug(isDebug){
    this.isDebug=!!isDebug;
    
    this.gui.domElement.style.display=this.isDebug?'':'none';
    $("#Stats").toggle(this.isDebug);
    $("html").toggleClass("debug",this.isDebug);
  }
  get debug(){
    return this.isDebug;
  }
  onSetRenderPassIndexByRelative(e,indexByRelative){
    if(IS_DEBUG){
      console.log("onSetRenderPassIndexByRelative",e,indexByRelative);
    }
    this.setRenderPassIndexByRelative(indexByRelative);
  }
  onSetParamByRelative(e,index,paramByRelative){
    if(IS_DEBUG){
      console.log("onSetParamByRelative",e,index,paramByRelative);
    }
    this.setParamByRelative(index,paramByRelative);
  }
  onSwitchChanged(e,index,value){
    if(IS_DEBUG){
      console.log("onSwitchChanged",e,index,value);
    }
    switch(index){
      case 0:
      this.automatic=value;
      break;
      case 1:
      this.power=value;
      break;
      default:
      //DO NOTHING
      break;
    }
  }
  
  onResize({width,height}){
    if(IS_DEBUG){
      console.log("onResize",{width,height})
    }
    
    this.pixiRenderer.resize(width,height);
  }
  onKeyDown(e){
    let window=remote.getCurrentWindow();
    console.log("onKeyDown",'"'+e.key+'"',e.keyCode)
    switch (e.key) {
      case "Escape":
      if (window.isFullScreen()) {
        window.setFullScreen(false);
      }else{
        window.close();
      }
      break;
      case "ArrowRight":
      this.nextRenderPass();
      break;
      case "ArrowLeft":
      this.previousRenderPass();
      break;
      case "z":
      this.automatic=!this.automatic;
      break;
      case "x":
      this.power=!this.power;
      break;
      case " ":
      this.debug=!this.debug;
      break;
      default:
      //DO NOTHING
      break;
    }
    let keyParamMap={
      q:{index:0,paramByRelative:-1},
      a:{index:0,paramByRelative:+1},
      w:{index:1,paramByRelative:-1},
      s:{index:1,paramByRelative:+1},
      e:{index:2,paramByRelative:-1},
      d:{index:2,paramByRelative:+1},
      r:{index:3,paramByRelative:-1},
      f:{index:3,paramByRelative:+1},
      t:{index:4,paramByRelative:-1},
      g:{index:4,paramByRelative:+1},
      y:{index:5,paramByRelative:-1},
      h:{index:5,paramByRelative:+1},
    };
    let keyParam=keyParamMap[e.key];
    if(keyParam){
      this.setParamByRelative(keyParam.index,keyParam.paramByRelative);
    }
  }
  onTick(){
    TWEEN.update();
    this.update();
    this.render();
  }
}

