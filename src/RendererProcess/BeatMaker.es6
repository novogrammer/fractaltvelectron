

export default class BeatMaker{
  constructor({bpm,timeStart}){
    this.bpm=bpm;
    this.timeStart=timeStart;
    this.timeNow=this.timeStart;
  }
  getElapsedTime(){
    return this.timeNow-this.timeStart;
  }
  toBeat(time,ratio=1){
    let bpm=this.bpm*ratio;
    let spb=60/bpm;
    let count=Math.floor(time/spb);
    let beatTime=count*spb;
    let fractionTimeOfBeat=time-beatTime;
    let remainTimeOfBeat=spb-fractionTimeOfBeat;
    return{
      bpm,
      spb,
      count,
      fractionTimeOfBeat,
      remainTimeOfBeat,
    };
  }
  getBeat(ratio=1){
    return this.toBeat(this.getElapsedTime(),ratio);
  }
  update(timeNow){
    this.timeNow=timeNow;
  }
}