import {
  POWERON_DURATION,
} from "../../constants";

//for extends
const PIXI=require('pixi.js');

export default class PowerOnFilter extends PIXI.filters.ColorMatrixFilter{
  constructor(){
    super();
    this.brightness(0);//black
    this.alpha=1;
    this.tween=new TWEEN.Tween(this)
    .to({alpha:0},POWERON_DURATION*1000);
  }
  start(){
    this.alpha=1;
    this.tween.start();
  }
  stop(){
    this.alpha=0;
    this.tween.stop();
  }
  
}