import {
  CHANNEL_DURATION,
} from "../../constants";
import {
  random,
} from "../../math_utils";

//for extends
const PIXI=require('pixi.js');

const {GlitchFilter}=require('@pixi/filter-glitch');
export default class ChannelGlitchFilter extends GlitchFilter{
  constructor(){
    super();
    this.fillMode=GlitchFilter.LOOP;
    this.slices=20;
    this.tweenTarget={
      redX:0,
      redY:0,
      greenX:0,
      greenY:0,
      blueX:0,
      blueY:0,
      offset:0,
    };
    this.tween=new TWEEN.Tween(this.tweenTarget).to({
      redX:0,
      redY:0,
      greenX:0,
      greenY:0,
      blueX:0,
      blueY:0,
      offset:0,
    },CHANNEL_DURATION*1000)
    .easing(TWEEN.Easing.Elastic.Out)
    .onUpdate((object)=>{
      this.red.x=object.redX;
      this.red.y=object.redY;
      this.green.x=object.greenX;
      this.green.y=object.greenY;
      this.blue.x=object.blueX;
      this.blue.y=object.blueY;
      this.offset=object.offset;
      this.seed=Math.random();
    });
    this.stop();
  }
  start(){
    let x=random(0,50);
    let y=random(0,50);
    this.tweenTarget.redX=x*0.6;
    this.tweenTarget.redY=y*0.6;
    this.tweenTarget.greenX=x*0.8;
    this.tweenTarget.greenY=y*0.8;
    this.tweenTarget.blueX=x*1;
    this.tweenTarget.blueY=y*1;
    this.tweenTarget.offset=random(-50,50);
    this.tween.start();
  }
  stop(){
    this.tween.stop();
    this.red.x=0;
    this.red.y=0;
    this.green.x=0;
    this.green.y=0;
    this.blue.x=0;
    this.blue.y=0;
    this.offset=0;
  }
  
}