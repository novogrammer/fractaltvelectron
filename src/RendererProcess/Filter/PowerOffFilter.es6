import {
  POWEROFF_DURATION,
  PIXI_FRAG_COMMON,
} from "../../constants";

//for extends
const PIXI=require('pixi.js');

const glsl=require("glslify");


export default class PowerOffFilter extends PIXI.Filter{
  constructor(){
    let uniforms={
      time:{type:'1f',value:0},
    };
    let fragmentShader=glsl(`
${PIXI_FRAG_COMMON}

uniform float time;

vec2 scale(vec2 uv,vec2 scaleFactor){
  vec2 center=(filterClamp.zw-filterClamp.xy)/2.0;
  vec2 scaledUv=(uv - center)/scaleFactor+center;
  return scaledUv;
}

void main(void)
{
  float minX=0.005;
  float minY=0.005;
  float phase1=0.1;
  float phase2=0.2;
  vec2 scaledUv;
  vec3 maxColor=vec3(1.0);
  vec3 additionalColor;
  if(time<phase1){
    float r=time/phase1;
    float y=(1.0-r)*(1.0-minY)+minY;
    scaledUv=scale(vTextureCoord,vec2(1.0,y));
    additionalColor=vec3(maxColor*r);
  }else if(time<phase1+phase2){
    float r=(time-phase1)/phase2;
    float x=(1.0-r)*(1.0-minX)+minX;
    scaledUv=scale(vTextureCoord,vec2(x,minY));
    additionalColor=maxColor;
  }else{
    discard;
  }

  if(
    scaledUv.x<filterClamp.x ||
    scaledUv.y<filterClamp.y ||
    filterClamp.z<scaledUv.x ||
    filterClamp.w<scaledUv.y
  ){
    discard;
  }else{
    vec4 color=texture2D(uSampler, scaledUv);
    gl_FragColor=color+vec4(additionalColor,0.0);
  }
}

`);
    super(null,fragmentShader,uniforms);
    this.padding=0;
    this.tween=new TWEEN.Tween(this.uniforms)
    .to({time:POWEROFF_DURATION},POWEROFF_DURATION*1000)
    .easing(TWEEN.Easing.Linear.None);
  }
  start(){
    this.uniforms.time=0;
    this.tween.start();
  }
  stop(){
    this.uniforms.time=0;
    this.tween.stop();
  }
  
}