function objectEquals(a,b){
  let keysA=Object.keys(a).sort();
  let keysB=Object.keys(b).sort();
  if(keysA.length!=keysB.length){
    return false;
  }
  for(let i=0;i<keysA.length;++i){
    let keyA=keysA[i];
    let keyB=keysB[i];
    if(keyA!=keyB){
      return false;
    }
    let valueA=a[keyA];
    let valueB=b[keyB];
    if(valueA!=valueB){
      return false;
    }
  }
  return true;
}

export default class FlyweightFactory{
  constructor(factory,keyComparerer=null){
    this.factory=factory;
    this.keyComparerer=keyComparerer||objectEquals;
    this.keyValueStore=[];
  }
  getValue(key){
    let keyToFind=key;
    for(let keyValue of this.keyValueStore){
      let {key,value}=keyValue;
      if(this.keyComparerer.call(null,keyToFind,key)){
        return value;
      }
    }
    
    let newValue=this.factory.call(null,key);
    this.keyValueStore.push({
      key:key,
      value:newValue,
    });
    return newValue;
  }
}