# フラクタルテレビ（Electron）
[フラクタルテレビ（Arduino）](https://bitbucket.org/novogrammer/fractaltvarduino/)と一緒に動かします。  
HOMEWORKS2018で展示していました。  
当日の説明用に置いていた文書はこちら[説明.md](https://bitbucket.org/novogrammer/fractaltvelectron/src/master/%E8%AA%AC%E6%98%8E.md)

## 頑張って動かす手順
constants.es6のIS_DEBUGとIS_STANDALONEをtrueに書き換えておく。
```
$ npm install
$ npm run electron-rebuild
$ npm run start
```
## 頑張って動かせた場合の操作（キーボード版）
* チャンネル切り替え 矢印キー ←/→ 
* パラメータ1 q/a
* パラメータ2 w/s
* パラメータ3 e/d
* パラメータ4 r/f
* パラメータ5 t/g
* パラメータ6 y/h
* 自動モード切り替え z
* 電源ボタン z
* デバッグモード切り替え Space

## 関係ありそうなリンク
* [動かしている様子の動画](https://www.instagram.com/p/BqhpaW3DmBA/)
* [インスタ部](http://instabu.net/)
* [HOMEWORKS2018アーカイブ（予定地）](http://instabu.net/homeworks2018/)

